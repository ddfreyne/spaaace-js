define(
  ['../../../core/core'],
  function (Core) {
    return {
      create: function (game) {
        var ship = game.model.ship;

        function render() {
        }

        var desiredAngle = (function () {
          var currentPos = game.model.currentStarSystem.position;
          var targetPos = game.model.targetStarSystem.position;
          return targetPos.vectorTo(currentPos).angle();
        })();

        function update(delta) {
          var msElapsed = (Date.now() - ship.hyperspaceEnteredAt) / 1000;

          // Turn
          var angleDiff = Core.Math.normalizeAngle(game.model.ship.angle - desiredAngle);
          if (Math.abs(angleDiff) > 0.1) {
            game.model.ship.turnTowardsAngle(desiredAngle, delta);
            game.model.update(delta);
            return;
          }

          // Accelerate
          game.model.ship.velocity.x += Math.cos(desiredAngle) * Math.pow(msElapsed / 2, 8);
          game.model.ship.velocity.y += Math.sin(desiredAngle) * Math.pow(msElapsed / 2, 8);
          game.model.update(delta);
          ship.hyperspaceProgress = Math.pow(msElapsed / 6, 10);

          // Done
          if (ship.hyperspaceProgress > 1.0) {
            game.model.currentStarSystem = game.model.targetStarSystem;
            game.model.targetStarSystem = null;
            game.model.gameConsole.log("Arrived in the " + game.model.currentStarSystem.name + " system.");

            game.model.ship.velocity.x = Math.cos(desiredAngle) * game.model.ship.shipType.maxVelocity;
            game.model.ship.velocity.y = Math.sin(desiredAngle) * game.model.ship.shipType.maxVelocity;

            game.model.ship.position.x = -Math.cos(desiredAngle) * 12000;
            game.model.ship.position.y = -Math.sin(desiredAngle) * 12000;

            game.stateStack.pop();
          }
        }

        function enter() {
          game.model.gameConsole.log("Entering hyperspace to the " + game.model.targetStarSystem.name + " system.");

          ship.hyperspaceEnteredAt = Date.now();
          ship.hyperspaceProgress = 0.0;
        }

        function exit() {
          game.model.resetShips();
          ship.hyperspaceEnteredAt = null;
          ship.hyperspaceProgress = 0.0;
        }

        var state = Core.State.create(true, update, render);
        state.enter = enter;
        state.exit = exit;

        return state;
      }
    };
  }
);
