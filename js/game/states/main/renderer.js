define(
  ['../../../core/core'],
  function (Core) {
    return {
      create: function (game, state) {
        var canvas        = game.canvas;
        var assetRegistry = game.assetRegistry;
        var ctx           = canvas.getContext("2d");

        var darkCyanColor     = "rgb(0,   23,  31)";
        var semidarkCyanColor = "rgb(0,   48,  63)";
        var cyanColor         = "rgb(0,   191, 255)";
        var whiteColor        = "rgb(255, 255, 255)";
        var grayColor         = "rgb(128, 175, 191)";
        var darkRedColor      = "rgb(48, 18, 18)";
        var orangeColor       = "#fc0";
        var redColor          = "#f33";

        var random = function () {
          var x = Math.sin(seed++) * 10000;
          return x - Math.floor(x);
        };

        var renderHyperspaceWhite = function () {
          if (game.model.ship.hyperspaceProgress) {
            ctx.fillStyle = "rgba(255, 255, 255, " + game.model.ship.hyperspaceProgress.toFixed(3) + ")";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
          }
        }

        var renderBackground = function () {
          ctx.fillStyle = "rgb(0, 0, 0)";
          ctx.fillRect(0, 0, canvas.width, canvas.height);
        };

        var hyperspaceShakeWhile = function (fn) {
          var progress = game.model.ship.hyperspaceProgress;
          if (progress) {
            ctx.save();

            var factor = 10 * Math.min(progress * 100.0, 1.0);
            var offsetX = Math.random() * factor;
            var offsetY = Math.random() * factor;
            ctx.translate(offsetX, offsetY);

            fn();

            ctx.restore();
          } else {
            fn();
          }
        };

        function localSeedFor(globalSeed, parallax, left, top) {
          return globalSeed + parallax*333 + left*10000 + top*100 + game.model.currentStarSystem.position.x + game.model.currentStarSystem.position.y;
        }

        var renderHugeStarfield = function (parallax, globalSeed, density, weight, viewWidth, viewHeight) {
          var translation = {};
          translation.x = positionWithParallax(parallax).x - viewWidth/2;
          translation.y = positionWithParallax(parallax).y - viewHeight/2;

          var left   = Math.floor(translation.x / viewWidth);
          var top    = Math.floor(translation.y / viewHeight);

          centerOnWhile(positionWithParallax(parallax), function () {
            ctx.save();
            ctx.translate(left*viewWidth, top*viewHeight);

            ctx.save();
            ctx.translate(0, 0);
            renderStarfield(localSeedFor(globalSeed, parallax, left, top), density, weight);
            ctx.restore();

            ctx.save();
            ctx.translate(0, viewHeight);
            renderStarfield(localSeedFor(globalSeed, parallax, left, top+1), density, weight);
            ctx.restore();

            ctx.save();
            ctx.translate(viewWidth, 0);
            renderStarfield(localSeedFor(globalSeed, parallax, left+1, top), density, weight);
            ctx.restore();

            ctx.save();
            ctx.translate(viewWidth, viewHeight);
            renderStarfield(localSeedFor(globalSeed, parallax, left+1, top+1), density, weight);
            ctx.restore();

            ctx.restore();
          });
        };

        var renderStarfield = function (localSeed, density, weight) {
          ctx.save();
          ctx.fillStyle = "white";
          seed = localSeed; // FIXME global!!!
          var amount = density * canvas.width * canvas.height;
          for(var i = 0; i < amount; i++) {
            var x = random() * canvas.width;
            var y = random() * canvas.height;
            var radius = random() * weight;
            ctx.beginPath();
            ctx.arc(x, y, radius, 0, Math.PI*2, false);
            ctx.closePath();
            ctx.fill();
          }
          ctx.restore();
        };

        var renderConsole = function () {
          ctx.save();

          ctx.fillStyle = "rgb(0, 191, 255)";
          ctx.font = "14px Clear Sans";
          var lines = game.model.gameConsole.lines;
          _.each(lines, function (line, i) {
            ctx.fillText(line, 10, 20 + i*20);
          });

          ctx.restore();
        };

        var formatNumber = function (num) {
          var tmp = Math.round(num * 10);

          if (num < 1000) {
            return '' + Math.round(num * 10) / 10;
          } else if (tmp < 1000000) {
            return '' + Math.round(num / 1000 * 10) / 10 + ' K';
          } else {
            return '' + Math.round(num / 1000000 * 10) / 10 + ' M';
          }
        };

        var renderHUD = function() {
          var w = 160;
          var h = 60;
          var x = canvas.width - w - 40;
          var y = 200 + 40;

          ctx.save();

          ctx.textBaseline = "middle";
          ctx.textAlign = "center";

          // credits

          renderBox(x, y, w, h, 'CRED', formatNumber(game.model.player.credits), 20);

          // capacity

          y += 120;
          var usedCapacity = 0;
          _.each(game.model.ship.commodities, function (count, key) {
            usedCapacity += count;
          });
          var text = usedCapacity + " / " + game.model.ship.capacity;
          renderBox(x, y, w, h, 'CAPA', text, 20);

          // current system

          y += 120;
          var currentName = game.model.currentStarSystem.name;
          renderBox(x, y, w, h, 'CURR', currentName, 16);

          // target system

          y += 120;
          var targetName = game.model.targetStarSystem ? game.model.targetStarSystem.name : "--";
          renderBox(x, y, w, h, 'TARG', targetName, 16);

          ctx.restore();
        };

        function renderBox(x, y, w, h, labelText, text, size) {
          ctx.beginPath();
          ctx.rect(x, y, w, h);
          ctx.fillStyle = darkCyanColor;
          ctx.fill();
          ctx.strokeStyle = semidarkCyanColor;
          ctx.stroke();

          ctx.fillStyle = whiteColor;
          ctx.font = size + "pt \"Clear Sans\"";
          var targetName = game.model.targetStarSystem ? game.model.targetStarSystem.name : "--";
          ctx.fillText(text, x + w/2, y + h/2);

          ctx.fillStyle = grayColor;
          ctx.font = "16pt \"Clear Sans\"";
          ctx.fillText(labelText, x + w/2, y + h*1.3);
        }

        var renderPlanet = function (planet) {
          ctx.save();

          ctx.translate(planet.position.x, planet.position.y);
          var planetImage = assetRegistry.image(planet.imageName);
          ctx.translate(-planetImage.width / 2, -planetImage.height / 2);

          ctx.drawImage(planetImage, 0, 0);

          ctx.restore();
        };

        var renderMinimap = function () {
          var diameter = 200;
          var offset = 10;
          var scale = 100;

          ctx.save();
          ctx.translate(canvas.width - diameter - offset, offset);
          ctx.translate(diameter/2, diameter/2);

          ctx.lineWidth = 3;

          ctx.beginPath();
          ctx.arc(0, 0, diameter/2, 0, Math.PI*2, false);
          ctx.closePath();
          ctx.fillStyle = darkCyanColor;
          ctx.fill();
          ctx.strokeStyle = cyanColor;
          ctx.stroke();

          ctx.lineWidth = 1.5;

          ctx.beginPath();
          ctx.arc(0, 0, diameter/2 * 0.75, 0, 2*Math.PI, false);
          ctx.strokeStyle = semidarkCyanColor;
          ctx.stroke();

          ctx.beginPath();
          ctx.arc(0, 0, diameter/2 * 0.5, 0, 2*Math.PI, false);
          ctx.strokeStyle = semidarkCyanColor;
          ctx.stroke();

          ctx.beginPath();
          ctx.arc(0, 0, diameter/2 * 0.25, 0, 2*Math.PI, false);
          ctx.strokeStyle = semidarkCyanColor;
          ctx.stroke();

          ctx.fillStyle = "white";
          ctx.beginPath();
          ctx.arc(0, 0, 2, 0, Math.PI*2, false);
          ctx.closePath();
          ctx.fill();

          var center = Core.Point.create(0, 0);

          ctx.fillStyle = orangeColor;
          game.model.currentStarSystem.planets.forEach(function (planet) {
            var point = Core.Point.create(
              (planet.position.x - game.model.ship.position.x) / scale,
              (planet.position.y - game.model.ship.position.y) / scale);
            if (point.distanceTo(center) > diameter/2) {
              return;
            }

            ctx.beginPath();
            ctx.arc(point.x, point.y, 2, 0, Math.PI*2, false);
            ctx.closePath();
            ctx.fill();
          });

          ctx.fillStyle = cyanColor;
          game.model.ships.forEach(function (ship) {
            var point = Core.Point.create(
              (ship.position.x - game.model.ship.position.x) / scale,
              (ship.position.y - game.model.ship.position.y) / scale);
            if (point.distanceTo(center) > diameter/2) {
              return;
            }

            ctx.beginPath();
            ctx.arc(point.x, point.y, 1, 0, Math.PI*2, false);
            ctx.closePath();
            ctx.fill();
          });

          ctx.restore();
        };

        var renderDebug = function () {
          ctx.save();
          ctx.translate(game.model.ship.position.x, game.model.ship.position.y);

          var drawVelocityVector  = false;
          var drawAngle           = false;
          var drawInverseVelocity = false;

          if (game.model.debugState && game.model.debugState > 0) {
            drawVelocityVector  = true;
          }
          if (game.model.debugState && game.model.debugState > 1) {
            drawAngle           = true;
          }
          if (game.model.debugState && game.model.debugState > 2) {
            drawInverseVelocity = true;
          }

          if (drawVelocityVector) {
            ctx.strokeStyle = 'rgb(0, 191, 255)';

            // Velocity vector
            ctx.beginPath();
            ctx.moveTo(0, 0);
            ctx.lineTo(game.model.ship.velocity.x * 50, game.model.ship.velocity.y * 50);
            ctx.stroke();
          }

          if (drawAngle) {
            ctx.strokeStyle = '#f00';

            var normalizedAngle = Core.Math.normalizeAngle(game.model.ship.angle);
            ctx.beginPath();
            ctx.moveTo(0, 0);
            ctx.arc(0, 0, 50, 0, normalizedAngle, normalizedAngle < 0);
            ctx.lineTo(0, 0);
            ctx.stroke();
          }

          if (drawInverseVelocity) {
            ctx.strokeStyle = '#00f';

            var inverseVelocity = Core.Vector.create(-game.model.ship.velocity.x, -game.model.ship.velocity.y);

            var shipAngle = Core.Math.normalizeAngle(game.model.ship.angle);
            var inverseVectorAngle = Core.Math.normalizeAngle(inverseVelocity.angle());
            var angleDiff = Core.Math.normalizeAngle(shipAngle - inverseVectorAngle);
            ctx.beginPath();
            ctx.moveTo(0, 0);
            ctx.arc(0, 0, 70, shipAngle, inverseVectorAngle, angleDiff > 0);
            ctx.lineTo(0, 0);
            ctx.stroke();
          }

          ctx.restore();
        };

        function positionWithParallax(parallax) {
          return Core.Point.create(
            game.model.ship.position.x / parallax,
            game.model.ship.position.y / parallax);
        }

        function centerOnShipWhile(fn) {
          centerOnWhile(game.model.ship.position, fn);
        }

        function centerOnWhile(point, fn) {
          ctx.save();

          ctx.translate(canvas.width / 2, canvas.height / 2);
          ctx.translate(-point.x, -point.y);

          fn();

          ctx.restore();
        }

        var initialSeed = Math.random();

        return {
          render: function () {
            renderBackground();

            hyperspaceShakeWhile(function () {
              // Dust
              renderHugeStarfield(1.1, initialSeed, 0.00003, 0.6, canvas.width, canvas.height);
              renderHugeStarfield(1.2, initialSeed, 0.00003, 0.6, canvas.width, canvas.height);
              renderHugeStarfield(1.4, initialSeed, 0.00002, 0.6, canvas.width, canvas.height);
              renderHugeStarfield(1.8, initialSeed, 0.00001, 0.6, canvas.width, canvas.height);
              renderHugeStarfield(2.6, initialSeed, 0.00001, 0.6, canvas.width, canvas.height);

              // Stars
              renderHugeStarfield(30,   initialSeed, 0.00001, 0.8, canvas.width, canvas.height);
              renderHugeStarfield(50,   initialSeed, 0.00002, 0.6, canvas.width, canvas.height);
              renderHugeStarfield(70,   initialSeed, 0.00003, 0.5, canvas.width, canvas.height);
              renderHugeStarfield(90,   initialSeed, 0.00005, 0.5, canvas.width, canvas.height);
              renderHugeStarfield(100,  initialSeed, 0.00002, 1.5, canvas.width, canvas.height);
              renderHugeStarfield(200,  initialSeed, 0.00002, 1.5, canvas.width, canvas.height);
              renderHugeStarfield(400,  initialSeed, 0.00002, 1.5, canvas.width, canvas.height);
              renderHugeStarfield(800,  initialSeed, 0.00002, 1.5, canvas.width, canvas.height);
              renderHugeStarfield(1600, initialSeed, 0.00002, 1.5, canvas.width, canvas.height);

              centerOnShipWhile(function () {
                game.model.currentStarSystem.planets.forEach(function (planet) {
                  renderPlanet(planet);
                });
                game.model.ships.forEach(function (ship) {
                  ship.render(ctx, assetRegistry);
                });
                game.model.ship.render(ctx, assetRegistry);
                renderDebug();
              });

              // Dust before
              renderHugeStarfield(0.9, initialSeed, 0.00002, 0.6, canvas.width, canvas.height);
              renderHugeStarfield(0.8, initialSeed, 0.00002, 0.6, canvas.width, canvas.height);

              renderConsole();
              renderMinimap();
              renderHUD();
            });

            renderHyperspaceWhite();
          }
        };
      }
    };
  }
);
