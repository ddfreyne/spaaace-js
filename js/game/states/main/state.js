define(
  ['../../../core/core', './controller', './renderer'],
  function (Core, MainController, MainRenderer) {
    return {
      create: function (game) {
        var controller;
        var renderer;

        function update(delta) {
          controller.update(delta);
        }

        function render() {
          renderer.render();
        }

        var state = Core.State.create(false, update, render);

        controller = MainController.create(game, state);
        renderer = MainRenderer.create(game, state);

        return state;
      }
    };
  }
);
