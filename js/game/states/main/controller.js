define(
  ['../../../core/core', '../planet/state', '../paused/state', '../map/state', '../help/state', '../hyperspace/state', '../../event_type'],
  function (Core, PlanetState, PausedState, MapState, HelpState, HyperspaceState, EventType) {
    function update(game, state, delta) {
      Core.Keys.handleKey(Core.Keys.KEY_L, function () {
        game.model.handleLandRequest();
      });

      Core.Keys.handleKey(Core.Keys.KEY_P, function () {
        game.stateStack.push(PausedState.create(game));
      });

      Core.Keys.handleKey(Core.Keys.KEY_M, function () {
        game.stateStack.push(MapState.create(game));
      });

      Core.Keys.handleKey(Core.Keys.KEY_H, function () {
        game.stateStack.push(HelpState.create(game));
      });

      Core.Keys.handleKey(Core.Keys.KEY_D, function () {
        if (!game.model.debugState) {
          game.model.debugState = 1;
        } else {
          game.model.debugState = (game.model.debugState + 1) % 4;
        }
      });

      Core.Keys.handleKey(Core.Keys.KEY_J, function () {
        if (!game.model.targetStarSystem) {
          game.model.gameConsole.log("Jump aborted; no target system set.");
          return;
        }

        var distanceToClosestObject = _.min(game.model.currentStarSystem.planets, function (planet) {
          return planet.position.distanceTo(game.model.ship.position);
        }).position.distanceTo(game.model.ship.position);
        if (distanceToClosestObject < 1000.0) {
          game.model.gameConsole.log("Jump aborted; massive object nearby.");
          return;
        }

        game.stateStack.push(HyperspaceState.create(game));
      });

      game.model.ship.isAccelerating               = Core.Keys.isKeyDown(Core.Keys.KEY_UP);
      game.model.ship.isTurningIntoReversePosition = Core.Keys.isKeyDown(Core.Keys.KEY_DOWN);
      game.model.ship.isTurningLeft                = Core.Keys.isKeyDown(Core.Keys.KEY_LEFT);
      game.model.ship.isTurningRight               = Core.Keys.isKeyDown(Core.Keys.KEY_RIGHT);

      game.model.eventQueue.forEach(function (event) {
        switch (event.name) {
          case EventType.SHIP_LANDED:
            game.stateStack.push(PlanetState.create(game));
            game.model.gameConsole.log("Welcome to " + event.params.planet.name + ", captain.");
            break;

          case EventType.SHIP_LANDING_PERMISSION_GRANTED:
            game.model.gameConsole.log("Landing permission granted.");
            break;

          case EventType.SHIP_LANDING_PERMISSION_DENIED_TOO_SPEEDY:
            game.model.gameConsole.log("Landing permission denied. Slow down.");
            break;

          case EventType.SHIP_LANDING_PERMISSION_DENIED_TOO_FAR_AWAY:
            game.model.gameConsole.log("Landing permission denied. Come closer.");
            break;
        }
      });

      game.model.update(delta);
    }

    return {
      create: function (game, state) {
        return {
          update: function (delta) { update(game, state, delta); }
        };
      }
    };
  }
);
