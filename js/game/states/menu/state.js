define(
  ['../../../core/core', '../main/state'],
  function (Core, MainState) {
    return {
      create: function (game) {
        var canvas = game.canvas;
        var ctx = canvas.getContext("2d");

        var menuSize = Core.Size.create(800, 600);

        function update(delta) {
        }

        // TODO move this to a rendering helper
        function eventPointRelativeToCanvas(event) {
          var canvasOffset = $(game.canvas).offset();
          return {
            x: event.pageX - canvasOffset.left,
            y: event.pageY - canvasOffset.top,
          };
        }

        // TODO move this to a rendering helper
        function scopeContextChangesWhile(fn) {
          ctx.save();
          fn();
          ctx.restore();
        }

        function translateToMenuRect(point) {
          return Core.Point.create(
            point.x - game.canvas.width/2  + menuSize.width/2,
            point.y - game.canvas.height/2 + menuSize.height/2
          );
        }

        function loadSavedModel() {
          var raw = localStorage['model'];
          if (!raw) {
            return null;
          }

          return JSON.parse(raw);
        }

        function handleEvent(name, event) {
          switch (name) {
            case "click":
              var point = translateToMenuRect(eventPointRelativeToCanvas(event));
              if (continueGameButtonRect().containsPoint(point)) {
                var savedModel = loadSavedModel();
                if (savedModel) {
                  game.model.load(savedModel);
                  game.stateStack.replace(MainState.create(game));
                }
                return;
              }
              if (newGameButtonRect().containsPoint(point)) {
                game.stateStack.replace(MainState.create(game));
                return;
              }
              break;
          }
        }

        function inCenterBoxWhile(fn) {
          scopeContextChangesWhile(function () {
            ctx.translate(
              canvas.width/2 - menuSize.width/2,
              canvas.height/2 - menuSize.height/2);

            ctx.strokeStyle = "rgb(0, 191, 255)";
            ctx.strokeRect(-0.5, -0.5, 800, 600);

            fn();
          });
        }

        function continueGameButtonRect() {
          var size   = Core.Size.create(240, 40);
          var origin = Core.Point.create(menuSize.width/2 - size.width/2, 260);

          return Core.Rect.create(origin, size);
        }

        function newGameButtonRect() {
          var size   = Core.Size.create(240, 40);
          var origin = Core.Point.create(menuSize.width/2 - size.width/2, 340);

          return Core.Rect.create(origin, size);
        }

        function renderButton(coords, isDisabled, text) {
          scopeContextChangesWhile(function () {
            // Background
            if (isDisabled) {
              ctx.fillStyle = "#090";
            } else {
              ctx.fillStyle = "rgb(0, 191, 255)";
            }
            ctx.fillRect(coords.origin.x, coords.origin.y, coords.size.width, coords.size.height);

            // Text
            ctx.textAlign = "center";
            ctx.fillStyle = "#000";
            ctx.font = "24px Clear Sans";
            ctx.textBaseline = "middle";
            ctx.fillText(text, coords.xCenter(), coords.yCenter());
          });
        }

        function render() {
          // background
          scopeContextChangesWhile(function () {
            ctx.globalAlpha = 0.7;
            ctx.fillStyle = "#000";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
          });

          inCenterBoxWhile(function () {
            // Title text
            scopeContextChangesWhile(function () {
              ctx.textAlign = "center";
              ctx.fillStyle = "rgb(0, 191, 255)";
              ctx.font = "36px Clear Sans";
              ctx.fillText("Welcome… to spaaace!", menuSize.width/2, 200);
            });

            // Buttons
            renderButton(continueGameButtonRect(), !loadSavedModel(), "Continue game");
            renderButton(newGameButtonRect(),      false,             "New game");
          });
        }

        var state = Core.State.create(true, update, render);
        state.handleEvent = handleEvent;

        return state;
      }
    };
  }
);
