define(
  ['../../../core/core'],
  function (Core) {
    return {
      create: function (game) {
        function render() {
          var canvas = game.canvas;
          var ctx = canvas.getContext("2d");

          ctx.save();
          ctx.globalAlpha = 0.5;
          ctx.fillStyle = "rgba(0, 0, 0)";
          ctx.fillRect(0, 0, canvas.width, canvas.height);
          ctx.restore();

          ctx.save();
          ctx.textAlign = "center";
          ctx.fillStyle = "rgb(0, 191, 255)";
          ctx.font = "24px Clear Sans";
          ctx.fillText(
            "[ Game paused — press escape to continue ]",
            canvas.width/2, canvas.height/2 - 12);
          ctx.restore();
        }

        function update(delta) {
          if (Core.Keys.isKeyDown(Core.Keys.KEY_ESC)) {
            game.stateStack.pop();
            return;
          }
        }

        var state = Core.State.create(true, update, render);

        return state;
      }
    };
  }
);
