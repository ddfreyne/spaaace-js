define(
  ['../../../core/core'],
  function (Core) {
    return {
      create: function (game) {
        function update(delta) {
          if (Core.Keys.isKeyDown(Core.Keys.KEY_ESC)) {
            game.stateStack.pop();
            return;
          }
        }

        function translatedEventCoordinates(event) {
          var canvasOffset = $(game.canvas).offset();
          return {
            x: event.pageX - canvasOffset.left,
            y: event.pageY - canvasOffset.top,
          };
        }

        function handleEvent(name, event) {
          switch (name) {
            case "click":
              var loc = translatedEventCoordinates(event);
              var mapOffset = offsetForCentering();
              var searchPoint = Core.Point.create(loc.x - mapOffset.x, loc.y - mapOffset.y);

              var closestSystem = _.min(game.model.starSystems, function (s) {
                return s.position.distanceTo(searchPoint);
              });
              if (closestSystem.position.distanceTo(searchPoint) < 20.0) {
                if (closestSystem !== game.model.currentStarSystem) {
                  game.model.targetStarSystem = closestSystem;
                }
              }

              break;
          }
        }

        function mapDimensions() {
          return {
            w: _.max(game.model.starSystems, function (s) { return s.position.x; }).position.x,
            h: _.max(game.model.starSystems, function (s) { return s.position.y; }).position.y,
          };
        }

        function offsetForCentering() {
          var dim = mapDimensions();
          return {
            x: game.canvas.width/2  - dim.w/2,
            y: game.canvas.height/2 - dim.h/2,
          };
        }

        function render() {
          var canvas = game.canvas;
          var ctx = canvas.getContext("2d");

          // background
          ctx.save();
          ctx.globalAlpha = 0.7;
          ctx.fillStyle = "#000";
          ctx.fillRect(0, 0, canvas.width, canvas.height);
          ctx.restore();

          // systems
          var offset = offsetForCentering();
          var scale = 1;
          ctx.save();
          ctx.translate(offset.x, offset.y);
          game.model.starSystems.forEach(function (starSystem) {
            var fillStyle;
            var strokeStyle;

            if (starSystem === game.model.currentStarSystem) {
              fillStyle = "#fff";
              strokeStyle = "#fff";
            } if (starSystem === game.model.targetStarSystem) {
              fillStyle = "#0cf";
              strokeStyle = "#0cf";
            }else {
              fillStyle = "rgb(0, 191, 255)";
            }

            ctx.fillStyle = fillStyle;
            ctx.beginPath();
            ctx.arc(starSystem.position.x, starSystem.position.y, 3, 0, Math.PI*2, false);
            ctx.closePath();
            ctx.fill();

            if (strokeStyle) {
              ctx.strokeStyle = strokeStyle;
              ctx.beginPath();
              ctx.arc(starSystem.position.x, starSystem.position.y, 7, 0, Math.PI*2, false);
              ctx.closePath();
              ctx.stroke();
            }
          });
          ctx.restore();
        }

        var state = Core.State.create(true, update, render);
        state.handleEvent = handleEvent;

        return state;
      }
    };
  }
);
