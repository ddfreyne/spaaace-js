define(
  ['../../../core/core'],
  function (Core) {
    return {
      create: function (game) {
        function render() {
          var canvas = game.canvas;
          var ctx = canvas.getContext("2d");

          ctx.clearRect(0, 0, canvas.width, canvas.height);

          ctx.save();
          ctx.textAlign = "center";
          ctx.fillStyle = "rgb(0, 191, 255)";
          ctx.font = "24px Clear Sans";
          ctx.fillText("[ Loading — please stand by ]", canvas.width/2, canvas.height/2 - 12);
          ctx.restore();
        }

        function update(delta) {
        }

        var state = Core.State.create(false, update, render);

        return state;
      }
    };
  }
);
