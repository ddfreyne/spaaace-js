define(
  ['../../../core/core'],
  function (Core) {
    return {
      create: function (game) {
        function update(delta) {
          if (Core.Keys.isKeyDown(Core.Keys.KEY_ESC)) {
            game.stateStack.pop();
            return;
          }
        }

        function render() {
          var canvas = game.canvas;
          var ctx = canvas.getContext("2d");

          // background
          ctx.save();
          ctx.globalAlpha = 0.7;
          ctx.fillStyle = "#000";
          ctx.fillRect(0, 0, canvas.width, canvas.height);
          ctx.restore();

          // Text
          ctx.save();
          ctx.translate(canvas.width/2 - 400, canvas.height/2 - 300);
          ctx.fillStyle = "rgb(0, 191, 255)";
          ctx.font = "14px Clear Sans";
          ctx.fillText("up",    0, 20); ctx.fillText("Accelerate",       60, 20);
          ctx.fillText("left",  0, 40); ctx.fillText("Turn left",        60, 40);
          ctx.fillText("right", 0, 60); ctx.fillText("Turn right",       60, 60);
          ctx.fillText("down",  0, 80); ctx.fillText("Turn around",      60, 80);

          ctx.fillText("D",     0, 120); ctx.fillText("Toggle debug drawing",       60, 120);
          ctx.fillText("H",     0, 140); ctx.fillText("Open help menu",             60, 140);
          ctx.fillText("J",     0, 160); ctx.fillText("Hyperjump to target system", 60, 160);
          ctx.fillText("L",     0, 180); ctx.fillText("Land on nearby planet",      60, 180);
          ctx.fillText("M",     0, 200); ctx.fillText("Open map",                   60, 200);
          ctx.fillText("P",     0, 220); ctx.fillText("Pause game",                 60, 220);
          ctx.restore();
        }

        var state = Core.State.create(true, update, render);

        return state;
      }
    };
  }
);
