define({
  create: function (game, state) {
    var canvas        = game.canvas;
    var assetRegistry = game.assetRegistry;
    var ctx           = canvas.getContext("2d");

    function renderOverlay() {
      ctx.save();

      ctx.globalAlpha = 0.5;

      ctx.fillStyle = "rgb(0,0,0)";
      ctx.fillRect(0, 0, canvas.width, canvas.height);

      ctx.restore();
    }

    function renderText() {
      var text = "[ Ship parked — press escape to take off ]";

      ctx.save();
      ctx.textAlign = "center";
      ctx.fillStyle = "rgb(0, 191, 255)";
      ctx.font = "24px Clear Sans";
      ctx.fillText(text, canvas.width/2, 120);
      ctx.restore();
    }

    function renderCommodityPrices() {
      ctx.save();

      var width = 600;
      var height = 400;

      ctx.translate(canvas.width/2, canvas.height/2);
      ctx.translate(-width/2, -height/2);

      ctx.beginPath();
      ctx.rect(-0.5, -0.5, width, height);

      ctx.strokeStyle = 'rgb(0, 191, 255)';
      ctx.stroke();

      ctx.textBaseline = "middle";
      ctx.font = "14px Clear Sans";

      // header
      ctx.fillStyle = "rgb(0, 191, 255)";
      ctx.fillRect(30, 20, width - 30*2, 20);
      ctx.fillStyle = "#000";
      ctx.fillText("Commodity", 40,  30);
      ctx.fillText("Price",     180, 30);
      ctx.fillText("Owned",     250, 30);

      // body
      ctx.fillStyle = "rgb(0, 191, 255)";
      var i = 0;
      var commodityNames = Object.keys(game.model.currentPlanet.commodityPrices).sort();
      commodityNames.forEach(function (commodityName) {
        var commodityPrice = game.model.currentPlanet.commodityPrices[commodityName];
        ctx.fillText(commodityName, 40,  70 + i*20);
        ctx.fillText(Math.round(commodityPrice), 180, 70 + i*20);

        var owned = game.model.ship.commodities[commodityName] || 0;
        ctx.fillText(owned, 250, 70 + i*20);

        if (state.localModel.selectedCommodityName === commodityName) {
          ctx.fillText(">", 30,  70 + i*20);
        }
        i++;
      });

      ctx.restore();
    }

    return {
      render: function () {
        renderOverlay();
        renderText();
        renderCommodityPrices();
      }
    };
  }
});
