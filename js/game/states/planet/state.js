define(
  ['../../../core/core', './controller', './renderer'],
  function (Core, PlanetController, PlanetRenderer) {
    return {
      create: function (game) {
        var controller;
        var renderer;

        function update(delta) {
          controller.update(delta);
        }

        function render() {
          renderer.render();
        }

        var state = Core.State.create(true, update, render);
        state.localModel = {
          selectedCommodity: null,
        };
        state.exit = function () {
          game.model.currentPlanet = null;
          game.model.resetShips();
        };
        state.enter = function () {
          var dumpedModel = game.model.dump();
          localStorage['model'] = JSON.stringify(dumpedModel);
        };

        controller = PlanetController.create(game, state);
        renderer   = PlanetRenderer.create(game, state);

        return state;
      }
    };
  }
);
