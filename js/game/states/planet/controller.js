define(
  ['../../../core/core'],
  function (Core) {
    return {
      create: function (game, state) {
        function leavePlanet() {
          game.model.gameConsole.log("Thank you for staying on " + game.model.currentPlanet.name + ". See you soon, captain!");
          game.stateStack.pop();
        }

        function down() {
          var commodityNames = Object.keys(game.model.currentPlanet.commodityPrices).sort();

          if (!state.localModel.selectedCommodityName) {
            state.localModel.selectedCommodityName = commodityNames[0];
          } else {
            var index = commodityNames.indexOf(state.localModel.selectedCommodityName);
            if (index < commodityNames.length-1) {
              state.localModel.selectedCommodityName = commodityNames[index+1];
            }
          }
        }

        function up() {
          var commodityNames = Object.keys(game.model.currentPlanet.commodityPrices).sort();

          if (!state.localModel.selectedCommodityName) {
            state.localModel.selectedCommodityName = commodityNames[commodityNames.length-1];
          } else {
            var index = commodityNames.indexOf(state.localModel.selectedCommodityName);
            if (index > 0) {
              state.localModel.selectedCommodityName = commodityNames[index-1];
            }
          }
        }

        function sell() {
          if (!state.localModel.selectedCommodityName) {
            return;
          }

          var owneds = game.model.ship.commodities;

          var name  = state.localModel.selectedCommodityName;
          var price = game.model.currentPlanet.commodityPrices[name];

          if (owneds[name] > 0) {
            owneds[name] = owneds[name] || 0;
            owneds[name]--;
            game.model.player.credits += price;
          }
        }

        function buy() {
          if (!state.localModel.selectedCommodityName) {
            return;
          }

          var owneds = game.model.ship.commodities;

          var usedCapacity = 0;
          _.each(owneds, function (count) {
            usedCapacity += count;
          });

          if (usedCapacity >= game.model.ship.capacity) {
            return;
          }

          var name  = state.localModel.selectedCommodityName;
          var price = game.model.currentPlanet.commodityPrices[name];

          if (game.model.player.credits > price) {
            owneds[name] = owneds[name] || 0;
            owneds[name]++;
            game.model.player.credits -= price;
          }
        }

        function update(delta) {
          if (Core.Keys.isKeyDown(Core.Keys.KEY_ESC)) {
            leavePlanet();
            return;
          }

          Core.Keys.handleKey(Core.Keys.KEY_DOWN, down);
          Core.Keys.handleKey(Core.Keys.KEY_UP, up);
          Core.Keys.handleKey(Core.Keys.KEY_B, buy);
          Core.Keys.handleKey(Core.Keys.KEY_S, sell);
        }

        return {
          update: update,
        };
      }
    };
  }
);
