define(
  [],
  function () {
    var MAX_LENGTH = 5;

    var proto = {
      dump: function () {
        return this.lines;
      },
      log: function (string) {
        if (this.lines.length === MAX_LENGTH) {
          this.lines.shift();
        }
        this.lines.push(string);
      }
    };

    return {
      load: function (data) {
        var self = Object.create(proto);
        self.lines = data;
        return self;
      },

      create: function () {
        var self = Object.create(proto);
        self.lines = [];
        return self;
      }
    };
  }
);
