define(
  [],
  function () {
    var proto = {
      dump: function () {
        return {
          maxTurnSpeed:  this.maxTurnSpeed,
          maxVelocity:   this.maxVelocity,
          imageName:     this.imageName,
          capacity:      this.capacity,
          imageRotation: this.imageRotation,
        };
      }
    };

    return {
      load: function (data) {
        var shipType = Object.create(proto);

        shipType.maxTurnSpeed  = data.maxTurnSpeed;
        shipType.maxVelocity   = data.maxVelocity;
        shipType.imageName     = data.imageName;
        shipType.capacity      = data.capacity;
        shipType.imageRotation = data.imageRotation;

        return shipType;
      },

      create: function () {
        var shipType = Object.create(proto);

        shipType.maxTurnSpeed  = 2.0;
        shipType.maxVelocity   = 10.0;
        shipType.imageName     = "ship1";
        shipType.capacity      = 10;
        shipType.imageRotation = Math.PI/2;

        return shipType;
      }
    };
  }
);
