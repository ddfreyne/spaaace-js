define(
  [],
  function () {
    var INITIAL_CREDITS = 1000;

    var proto = {
      dump: function () {
        return {
          credits: this.credits
        };
      }
    };

    return {
      load: function (data) {
        var obj = Object.create(proto);
        obj.credits = data.credits;
        return obj;
      },

      create: function () {
        var obj = Object.create(proto);
        obj.credits = INITIAL_CREDITS;
        return obj;
      }
    };
  }
);
