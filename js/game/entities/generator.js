define(function(require, exports, module) {
  var Core       = require('../../core/core');
  var Player     = require('./player');
  var Ship       = require('./ship');
  var ShipType   = require('./ship_type');
  var Names      = require('./names');
  var ShipAI     = require('./ship_ai');
  var Planet     = require('./planet');
  var StarSystem = require('./star_system');
  var Console    = require('./console');
  var EventType  = require('../event_type');

  // TODO keep the new planet away from other planets, and systems away from other systems

  function newRandomPlanet(model) {
    var name = Names.random();
    var imageR = Math.floor(Math.random() * 17);
    var x = Math.random() * 10000 - 5000;
    var y = Math.random() * 10000 - 5000;

    return Planet.create(x, y, name, 'planet-' + imageR);
  }

  function newRandomPlanets(min, max, model) {
    var planets = [];
    var numPlanets = min + Math.floor(Math.random() * (max - min));
    for (var i = 0; i < numPlanets; i++) {
      planets.push(newRandomPlanet());
    }
    return planets;
  }

  function newRandomStarSystem(model) {
    var name = Names.random();
    var x = Math.random() * 500 - 250;
    var y = Math.random() * 500 - 250;
    var planets = newRandomPlanets(1, 10);
    return StarSystem.create(x, y, name, planets);
  }

  function newRandomStarSystems(min, max, model) {
    var starSystems = [];
    var numStarSystem = min + Math.floor(Math.random() * (max - min));
    for (var i = 0; i < numStarSystem; i++) {
      starSystems.push(newRandomStarSystem());
    }
    return starSystems;
  }

  function newRandomShipType(model) {
    var shipType = ShipType.create();
    var min = 1;
    var max = 29;
    var num = min + Math.floor(Math.random() * (max - min));
    shipType.imageName = 'ship' + num;
    if (num < 10) {
      shipType.imageRotation = Math.PI/2;
    } else {
      shipType.imageRotation = -Math.PI/2;
    }
    return shipType;
  }

  function newRandomShips(min, max, model) {
    var ships = [];
    var numShips = min + Math.floor(Math.random() * (max - min));
    for (var i = 0; i < numShips; i++) {
      ships.push(newRandomShip(model));
    }
    return ships;
  }

  function newRandomShip(model) {
    var shipType = newRandomShipType();
    var ship = Ship.create(shipType);

    var minX = -4000;
    var minY = -4000;
    var maxX = 4000;
    var maxY = 4000;
    ship.position.x = minX + Math.floor(Math.random() * (maxX - minX));
    ship.position.y = minY + Math.floor(Math.random() * (maxY - minY));

    ship.angle      = -Math.PI + Math.random()*2*Math.PI;

    var minVel = 1.0;
    var maxVel = 3.0;
    var vel = minVel + Math.random() * (maxVel - minVel)

    ship.velocity.x = Math.cos(ship.angle) * vel;
    ship.velocity.y = Math.sin(ship.angle) * vel;

    ship.ai = ShipAI.create(ship, model);

    return ship;
  }

  return {
    create: function (model) {
      return {
        newRandomPlanet:      function()         { return newRandomPlanet(model);                },
        newRandomPlanets:     function(min, max) { return newRandomPlanets(min, max, model);     },
        newRandomStarSystem:  function()         { return newRandomStarSystem();                 },
        newRandomStarSystems: function(min, max) { return newRandomStarSystems(min, max, model); },
        newRandomShipType:    function()         { return newRandomShipType(model);              },
        newRandomShips:       function(min, max) { return newRandomShips(min, max, model);       },
        newRandomShip:        function()         { return newRandomShip(model);                  }
      }
    }
  };
});
