define(
  ['../../core/core'],
  function (Core) {
    var MAX_PRICE = 1000;

    var proto = {
      dump: function () {
        return {
          position:        this.position.dump(),
          name:            this.name,
          imageName:       this.imageName,
          commodityPrices: this.commodityPrices,
        };
      }
    };

    function generateRandomCommodities() {
      return {
        iron:     Math.random() * MAX_PRICE,
        food:     Math.random() * MAX_PRICE,
        uranium:  Math.random() * MAX_PRICE,
        medicine: Math.random() * MAX_PRICE,
        drugs:    Math.random() * MAX_PRICE,
      };
    }

    return {
      load: function (data) {
        var planet = Object.create(proto);

        planet.position        = Core.Point.load(data.position);
        planet.name            = data.name;
        planet.imageName       = data.imageName;
        planet.commodityPrices = data.commodityPrices;

        return planet;
      },

      create: function (x, y, name, imageName) {
        var planet = Object.create(proto);

        planet.position        = Core.Point.create(x, y);
        planet.name            = name;
        planet.imageName       = imageName;
        planet.commodityPrices = generateRandomCommodities();

        return planet;
      }
    };
  }
);
