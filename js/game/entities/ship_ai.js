define(
  ['../../core/core'],
  function (Core) {
    function randomPlanet(model, excludedPlanet) {
      var planets = model.currentStarSystem.planets;
      var index = 0 + Math.floor(Math.random() * planets.length);
      var planet = planets[index];
      if (planet === excludedPlanet && planets.length > 1) {
        return randomPlanet(model, excludedPlanet);
      } else {
        return planet;
      }
    }

    var proto = {
      dump: function () {
        // TODO implement

        return {
        };
      },

      update: function (delta) {
        if (this.targetPlanet) {
          var directionVector = this.targetPlanet.position.vectorTo(this.ship.position);
          var velocityVector  = this.ship.velocity;

          var correctedVector = directionVector.dup();
          correctedVector.subtract(velocityVector.dup().scalarMultiply(20));

          var distance = directionVector.length();

          var directionAngle = correctedVector.angle();

          if (distance < 200) {
            this.targetPlanet = randomPlanet(this.model, this.targetPlanet);
          }

          this.ship.turnTowardsAngle(directionAngle, delta);
          if (Core.Math.normalizeAngle(Math.abs(this.ship.angle - directionAngle)) < Math.PI/8) {
            this.ship.isAccelerating = true;
          } else {
            this.ship.isAccelerating = false;
          }
        } else {
          // Find a target planet
          this.targetPlanet = randomPlanet(this.model, this.targetPlanet);
        }
      },
    };

    return {
      load: function (data) {
        var shipAI = Object.create(proto);

        // TODO implement

        return shipAI;
      },

      create: function (ship, model) {
        var shipAI = Object.create(proto);

        shipAI.ship = ship;
        shipAI.model = model;

        shipAI.targetPlanet = null;

        return shipAI;
      }
    };
  }
);
