define(
  ['../../core/core', './planet'],
  function (Core, Planet) {
    var proto = {
      dump: function () {
        return {
          position: this.position.dump(),
          name:     this.name,
          planets:  _.map(this.planets, function (e) { return e.dump(); }),
        };
      }
    };

    return {
      load: function (data) {
        var system = Object.create(proto);

        system.position = Core.Point.load(data.position);
        system.name     = data.name;
        system.planets  = _.map(data.planets, Planet.load);

        return system;
      },

      create: function (x, y, name, planets) {
        var system = Object.create(proto);

        system.position = Core.Point.create(x, y);
        system.name     = name;
        system.planets  = planets;

        return system;
      }
    };
  }
);
