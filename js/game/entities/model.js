define(function(require, exports, module) {
  var Core       = require('../../core/core');
  var Player     = require('./player');
  var Ship       = require('./ship');
  var ShipType   = require('./ship_type');
  var Names      = require('./names');
  var ShipAI     = require('./ship_ai');
  var Planet     = require('./planet');
  var StarSystem = require('./star_system');
  var Console    = require('./console');
  var EventType  = require('../event_type');
  var Generator  = require('./generator');

  function dump(model) {
    return {
      gameConsole: model.gameConsole.dump(),
      eventQueue:  model.eventQueue.dump(),
      shipType:    model.shipType.dump(),
      ship:        model.ship.dump(),
      player:      model.player.dump(),
      starSystems: _.map(model.starSystems, function (e) { return e.dump(); }),

      // TODO dump ships

      currentPlanetIdx:     model.currentStarSystem.planets.indexOf(model.currentPlanet),
      currentStarSystemIdx: model.starSystems.indexOf(model.currentStarSystem),
      targetStarSystemIdx:  model.starSystems.indexOf(model.targetStarSystem),
    };
  }

  function load(model, data) {
    model.gameConsole = Console.load(data.gameConsole);
    model.eventQueue  = Core.EventQueue.load(data.eventQueue);
    model.shipType    = ShipType.load(data.shipType);
    model.ship        = Ship.load(data.ship, model);
    model.player      = Player.load(data.player);
    model.starSystems = _.map(data.starSystems, StarSystem.load);

    // TODO load ships

    model.currentStarSystem = model.starSystems[data.currentStarSystemIdx];
    model.currentPlanet     = model.currentStarSystem.planets[data.currentPlanetIdx];
    model.targetStarSystem  = model.starSystems[data.targetStarSystemIdx];
  }

  function update(model, delta) {
    model.ship.update(delta);
    model.ships.forEach(function (ship) {
      ship.update(delta);
    })
  };

  return {
    create: function () {
      function setup() {
        model.gen = Generator.create(model);
        model.gameConsole       = Console.create();
        model.eventQueue        = Core.EventQueue.create();
        model.shipType          = ShipType.create();
        model.ship              = Ship.create(model.shipType);
        model.player            = Player.create();
        model.currentPlanet     = null;
        model.starSystems       = model.gen.newRandomStarSystems(10, 20);
        model.currentStarSystem = model.starSystems[0];
        model.targetStarSystem  = null;
        model.ships             = model.gen.newRandomShips(10, 40);
      }

      var handleLandRequest = function () {
        var closestPlanet = _.min(model.currentStarSystem.planets, function (planet) {
          return planet.position.distanceTo(model.ship.position);
        });

        if (closestPlanet.position.distanceTo(model.ship.position) < 150.0) {
          if (model.ship.velocity.length() > 2.0) {
            model.eventQueue.emit(EventType.SHIP_LANDING_PERMISSION_DENIED_TOO_SPEEDY, {});
          } else {
            model.ship.position.x = closestPlanet.position.x;
            model.ship.position.y = closestPlanet.position.y;
            model.ship.velocity.x = 0;
            model.ship.velocity.y = 0;
            model.currentPlanet = closestPlanet;

            model.eventQueue.emit(EventType.SHIP_LANDED, {planet: closestPlanet});
          }
        }
      };

      var model = {
        setup: setup,
        load:  function(data)   { load(model, data) },
        dump:  function()       { return dump(model) },
        update: function(delta) { update(model, delta) },
        resetShips: function()  { model.ships = model.gen.newRandomShips(10, 40); },
        handleLandRequest: handleLandRequest,
      };

      return model;
    }
  };
});
