define(
  ['../../core/core'],
  function (Core) {
    // TODO move this into a helper module
    function limitTurnSpeed(angle, limit) {
      if (angle > limit) {
        return limit;
      } else if (angle < -limit) {
        return -limit;
      } else {
        return angle;
      }
    }

    function accelerate(ship, delta) {
      var cos = Math.cos(ship.angle);
      var sin = Math.sin(ship.angle);

      ship.velocity.x += cos * delta * ship.acceleration;
      ship.velocity.y += sin * delta * ship.acceleration;

      ship.velocity.limitToLength(ship.shipType.maxVelocity);
    }

    function turnIntoReversePosition(ship, delta) {
      ship.turnTowardsAngle(ship.velocity.angle() - Math.PI, delta);
    }

    var proto = {
      dump: function () {
        return {
          position:                     this.position.dump(),
          velocity:                     this.velocity.dump(),
          angle:                        this.angle,
          acceleration:                 this.acceleration,
          commodities:                  this.commodities,
          capacity:                     this.capacity,
          isAccelerating:               this.isAccelerating,
          isTurningIntoReversePosition: this.isTurningIntoReversePosition,
          isTurningLeft:                this.isTurningLeft,
          isTurningRight:               this.isTurningRight,
        };
      },

      // TODO pass in renderer instead
      render: function (ctx, assetRegistry) {
        ctx.save();
        ctx.translate(this.position.x, this.position.y);

        var shipImage = assetRegistry.image(this.shipType.imageName);

        // Thrusters
        if (this.isAccelerating) {
          ctx.save();
          ctx.rotate(this.angle);
          ctx.scale(shipImage.width / 5, shipImage.height / 5);

          ctx.beginPath();
          ctx.lineTo(-0.5,  0.5);
          ctx.lineTo(-0.5, -0.5);
          ctx.lineTo(-1.5 + Math.sin(this.thrustProgress * 50) * 0.2,  0);
          ctx.closePath();
          ctx.fillStyle = "#fc0";
          ctx.fill();

          ctx.beginPath();
          ctx.lineTo(-0.5,  0.5);
          ctx.lineTo(-0.5, -0.5);
          ctx.lineTo(-1.2 + Math.sin(this.thrustProgress * 70) * 0.1,  0);
          ctx.closePath();
          ctx.fillStyle = "#f60";
          ctx.fill();

          ctx.restore();
        };

        // Hyperspace
        if (this.hyperspaceEnteredAt) {
          ctx.save();
          ctx.rotate(this.angle);

          var width = 400;
          var height  = shipImage.height / 3;

          var alpha = 1 - Math.pow(1 - this.hyperspaceProgress, 10).toFixed(3);

          ctx.beginPath();
          ctx.rect(-width, -height / 2, width, height);
          var gradient = ctx.createLinearGradient(0, 0, -width, 0);
          gradient.addColorStop(0, "rgba(0, 192, 255, " + alpha + ")");
          gradient.addColorStop(1, "rgba(255, 255, 255, 0)");
          ctx.fillStyle = gradient;
          ctx.fill();

          ctx.beginPath();
          ctx.rect(-width, -height * 0.7 / 2, width, height * 0.7);
          var gradient = ctx.createLinearGradient(0, 0, -width, 0);
          gradient.addColorStop(0, "rgba(64, 224, 255, " + alpha + ")");
          gradient.addColorStop(1, "rgba(255, 255, 255, 0)");
          ctx.fillStyle = gradient;
          ctx.fill();

          ctx.beginPath();
          ctx.rect(-width, -height * 0.4 / 2, width, height * 0.4);
          var gradient = ctx.createLinearGradient(0, 0, -width, 0);
          gradient.addColorStop(0, "rgba(128, 240, 255, " + alpha + ")");
          gradient.addColorStop(1, "rgba(255, 255, 255, 0)");
          ctx.fillStyle = gradient;
          ctx.fill();

          ctx.restore();
        }

        // Image
        ctx.save();
        ctx.scale(0.5, 0.5);
        ctx.rotate(this.angle + this.shipType.imageRotation);
        ctx.translate(-shipImage.width / 2, -shipImage.height / 2);
        ctx.drawImage(shipImage, 0, 0);
        ctx.restore();

        ctx.restore();
      },

      update: function (delta) {
        if (this.ai) {
          this.ai.update(delta);
        }

        if (this.isTurningLeft) {
          this.angle -= delta * this.shipType.maxTurnSpeed;
        }

        if (this.isTurningRight) {
          this.angle += delta * this.shipType.maxTurnSpeed;
        }

        if (this.isAccelerating) {
          accelerate(this, delta);
          this.thrustProgress += delta;
        } else {
          this.thrustProgress = 0;
        }

        if (this.isTurningIntoReversePosition) {
          turnIntoReversePosition(this, delta);
        }

        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;
      },

      turnTowardsAngle: function (desiredAngle, delta) {
        var invVelocityAngle = Core.Math.normalizeAngle(desiredAngle);
        var angleDiff = Core.Math.normalizeAngle(invVelocityAngle - this.angle);
        var diff = limitTurnSpeed(angleDiff, delta * this.shipType.maxTurnSpeed);
        this.angle += diff;
      }
    };

    return {
      load: function (data, model) {
        var ship = Object.create(proto);

        ship.shipType                     = model.shipType;
        ship.position                     = Core.Point.load(data.position);
        ship.velocity                     = Core.Vector.load(data.velocity);
        ship.angle                        = data.angle;
        ship.acceleration                 = data.acceleration;
        ship.commodities                  = data.commodities;
        ship.capacity                     = data.capacity;
        ship.isAccelerating               = data.isAccelerating;
        ship.isTurningIntoReversePosition = data.isTurningIntoReversePosition;
        ship.isTurningLeft                = data.isTurningLeft;
        ship.isTurningRight               = data.isTurningRight;

        return ship;
      },

      create: function (shipType) {
        var ship = Object.create(proto);

        ship.shipType                     = shipType;
        ship.position                     = Core.Point.create(200, 200);
        ship.velocity                     = Core.Vector.create(0, 0);
        ship.angle                        = 0;
        ship.acceleration                 = 10.0;
        ship.commodities                  = {};
        ship.capacity                     = 10;
        ship.isAccelerating               = false;
        ship.isTurningIntoReversePosition = false;
        ship.isTurningLeft                = false;
        ship.isTurningRight               = false;

        ship.thrustProgress = 0.0;

        return ship;
      }
    };
  }
);
