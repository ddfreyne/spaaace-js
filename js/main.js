define(
  ['core/core', 'game/entities/model', 'game/states/menu/state', 'game/states/loading/state'],
  function (Core, World, MenuState, LoadingState) {
    if (!Modernizr.localstorage || !Modernizr.canvas) {
      document.write("Sorry, your browser does not support either canvas or local storage.");
      return;
    }

    var canvas = document.createElement("canvas");
    canvas.width  = 1200;
    canvas.height = 800;
    document.body.appendChild(canvas);

    var assetRegistry = Core.AssetRegistry.create();
    var model = World.create();
    var game = Core.Game.create(window, canvas, model, assetRegistry);

    game.stateStack.push(LoadingState.create(game));
    game.run();

    // Global object, to be able to access it from the console
    Game = game;

    assetRegistry.register("ship1", "img/ships/playerShip1_blue.png");
    assetRegistry.register("ship2", "img/ships/playerShip2_blue.png");
    assetRegistry.register("ship3", "img/ships/playerShip3_blue.png");
    assetRegistry.register("ship4", "img/ships/playerShip1_red.png");
    assetRegistry.register("ship5", "img/ships/playerShip2_red.png");
    assetRegistry.register("ship6", "img/ships/playerShip3_red.png");
    assetRegistry.register("ship7", "img/ships/playerShip1_green.png");
    assetRegistry.register("ship8", "img/ships/playerShip2_green.png");
    assetRegistry.register("ship9", "img/ships/playerShip3_green.png");

    assetRegistry.register("ship10", "img/ships/enemyRed1.png");
    assetRegistry.register("ship11", "img/ships/enemyRed2.png");
    assetRegistry.register("ship12", "img/ships/enemyRed3.png");
    assetRegistry.register("ship13", "img/ships/enemyRed4.png");
    assetRegistry.register("ship14", "img/ships/enemyRed5.png");

    assetRegistry.register("ship15", "img/ships/enemyGreen1.png");
    assetRegistry.register("ship16", "img/ships/enemyGreen2.png");
    assetRegistry.register("ship17", "img/ships/enemyGreen3.png");
    assetRegistry.register("ship18", "img/ships/enemyGreen4.png");
    assetRegistry.register("ship19", "img/ships/enemyGreen5.png");

    assetRegistry.register("ship20", "img/ships/enemyBlue1.png");
    assetRegistry.register("ship21", "img/ships/enemyBlue2.png");
    assetRegistry.register("ship22", "img/ships/enemyBlue3.png");
    assetRegistry.register("ship23", "img/ships/enemyBlue4.png");
    assetRegistry.register("ship24", "img/ships/enemyBlue5.png");

    assetRegistry.register("ship25", "img/ships/enemyBlack1.png");
    assetRegistry.register("ship26", "img/ships/enemyBlack2.png");
    assetRegistry.register("ship27", "img/ships/enemyBlack3.png");
    assetRegistry.register("ship28", "img/ships/enemyBlack4.png");
    assetRegistry.register("ship29", "img/ships/enemyBlack5.png");

    for (var i = 0; i < 18; i++) {
      assetRegistry.register("planet-" + i, "img/planets/" + i + ".png");
    }

    assetRegistry.onFinish(function () {
      model.setup();
      game.stateStack.replace(MenuState.create(game));
    });

    assetRegistry.load();
  }
);
