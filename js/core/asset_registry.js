define({
  create: function () {
    var urlRegistry = {};
    var imageRegistry = {};
    var loadCount = 0;
    var onFinish;

    var decreaseLoadCount = function () {
      loadCount--;
      if (loadCount === 0) {
        onFinish();
      }
    };

    var register = function (name, url) {
      urlRegistry[name] = url;
    };

    var load = function () {
      _.each(urlRegistry, function (url, name) {
        var image = new Image();
        imageRegistry[name] = image;
        loadCount++;
        image.onload = decreaseLoadCount;
        image.src = url;
      });
    };

    var setOnFinish = function (fn) {
      onFinish = fn;
    };

    var getImageNamed = function (name) {
      return imageRegistry[name];
    };

    var res = {
      register: register,
      load: load,
      onFinish: setOnFinish,
      image: getImageNamed,
    };

    return res;
  }
});
