define(
  [],
  function () {
    var mod = {
      load: function (data) {
        return mod.create(data);
      },

      create: function (initialEvents) {
        var events = initialEvents || [];

        function emit(name, params) {
          events.push({name: name, params: params});
        }

        function forEach(fn) {
          events.forEach(fn);
          events.length = 0;
        }

        function dump() {
          // FIXME events can have references to other objects
          return events;
        }

        return {
          emit: emit,
          dump: dump,
          forEach: forEach,
        };
      }
    };

    return mod;
  }
);
