define({
  create: function (isTransparent, update, render) {
    return {
      isTransparent: isTransparent,
      update: update,
      render: render,
      enter: function () {},
      exit: function () {},
      handleEvent: function (name, event) {},
    };
  }
});
