define({
  create: function () {
    var stack = [];

    function push(state) {
      stack.push(state);
      if (state.enter) {
        state.enter();
      }
    }

    function pop() {
      var state = top();
      if (state && state.exit) {
        state.exit();
      }
      return stack.pop();
    }

    function replace(state) {
      stack.pop();
      stack.push(state);
    }

    function top() {
      return stack[stack.length - 1];
    }

    function allVisible() {
      var visible = [];
      var done = false;
      for (var i = stack.length-1; i >= 0; i--) {
        var state = stack[i];

        if (done) {
          break;
        }

        visible.push(state);

        if (!state.isTransparent) {
          done = true;
        }
      }
      visible.reverse();
      return visible;
    }

    return {
      push: push,
      pop: pop,
      top: top,
      replace: replace,
      allVisible: allVisible,
    };
  }
});
