define(
  ['./state_stack'],
  function (StateStack) {
    return {
      create: function (window, canvas, model, assetRegistry) {
        var then = Date.now();
        var stateStack = StateStack.create();

        function step() {
          window.requestAnimationFrame(step);

          var now = Date.now();
          var bigDelta = now - then;

          stateStack.allVisible().forEach(function (state) {
            state.render();
            if (state === stateStack.top()) {
              state.update(bigDelta / 1000);
            }
          });

          then = now;
        }

        function eventHandler(name) {
          return function (event) {
            stateStack.top().handleEvent(name, event);
          };
        }

        canvas.addEventListener("click", eventHandler("click"), false);

        function run() {
          then = Date.now();
          step();
        }

        return {
          run: run,
          canvas: canvas,
          model: model,
          assetRegistry: assetRegistry,
          stateStack: stateStack,
        };
      }
    };
  }
);
