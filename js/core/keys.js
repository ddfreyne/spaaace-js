define(function () {
  var handleKey = function (key, fn) {
    if (key in keysDown) {
      delete keysDown[key];
      fn();
    }
  };

  var keysDown = {};

  var isKeyDown = function (key) {
    return key in keysDown;
  };

  var mod = {
    KEY_LEFT:  37,
    KEY_UP:    38,
    KEY_RIGHT: 39,
    KEY_DOWN:  40,

    KEY_ESC:   27,
    KEY_ENTER: 13, // FIXME enter? return? both?

    KEY_B:     66,
    KEY_D:     68,
    KEY_H:     72,
    KEY_J:     74,
    KEY_L:     76,
    KEY_M:     77,
    KEY_P:     80,
    KEY_S:     83,

    handleKey: handleKey,
    isKeyDown: isKeyDown,
  };

  addEventListener("keydown", function (e) { keysDown[e.keyCode] = true; }, false);
  addEventListener("keyup",   function (e) { delete keysDown[e.keyCode]; }, false);

  return mod;
});
