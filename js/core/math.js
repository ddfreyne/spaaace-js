define(function () {
  var mod = {
    normalizeAngle: function (angle) {
      if (angle < -Math.PI) {
        return mod.normalizeAngle(angle + 2*Math.PI);
      } else if (angle > Math.PI) {
        return mod.normalizeAngle(angle - 2*Math.PI);
      } else {
        return angle;
      }
    },
  };

  return mod;
});
