define(
  ['./asset_registry', './game', './state', './state_stack', './keys', './math', './event_queue',
  './helper/point', './helper/vector', './helper/size', './helper/rect'],
  function (AssetRegistry, Game, State, StateStack, Keys, CoreMath, EventQueue, Point, Vector, Size, Rect) {
    return {
      AssetRegistry: AssetRegistry,
      Game: Game,
      State: State,
      StateStack: StateStack,
      Keys: Keys,
      Math: CoreMath,
      Point: Point,
      Vector: Vector,
      Size: Size,
      Rect: Rect,
      EventQueue: EventQueue,
    };
  }
);
