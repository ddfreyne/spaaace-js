define(
  ['./point', './size'],
  function (Point, Size) {
    var proto = {
      containsPoint: function (point) {
        return this.left() <= point.x && point.x <= this.right() &&
               this.top()  <= point.y && point.y <= this.bottom();
      },

      dump: function () {
        return {
          origin: this.origin.dump(),
          size:   this.size.dump(),
        };
      },

      top: function () {
        return this.origin.y;
      },

      bottom: function () {
        return this.origin.y + this.size.height;
      },

      left: function () {
        return this.origin.x;
      },

      right: function () {
        return this.origin.x + this.size.width;
      },

      xCenter: function () {
        return this.origin.x + this.size.width/2;
      },

      yCenter: function () {
        return this.origin.y + this.size.height/2;
      },
    };

    return {
      load: function (data) {
        var rect = Object.create(proto);
        rect.origin = Point.load(data.origin);
        rect.size   = Size.load(data.size);
        return rect;
      },

      create: function (origin, size) {
        var rect = Object.create(proto);
        rect.origin = origin;
        rect.size   = size;
        return rect;
      }
    };
  }
);
