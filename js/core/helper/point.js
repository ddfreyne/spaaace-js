define(
  ['./vector'],
  function (Vector) {
    var proto = {
      dump: function () {
        return {
          x: this.x,
          y: this.y,
        };
      },

      distanceTo: function (other) {
        return this.vectorTo(other).length();
      },

      vectorTo: function (other) {
        return Vector.create(this.x - other.x, this.y - other.y);
      }
    };

    return {
      load: function (data) {
        var point = Object.create(proto);
        point.x = data.x;
        point.y = data.y;
        return point;
      },

      create: function (x, y) {
        var point = Object.create(proto);
        point.x = x;
        point.y = y;
        return point;
      }
    };
  }
);
