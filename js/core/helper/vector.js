define(
  [],
  function () {
    var proto = {
      dump: function () {
        return {
          x: this.x,
          y: this.y,
        };
      },

      length: function () {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
      },

      setLength: function (length) {
        var factor = length / this.length();
        this.x *= factor;
        this.y *= factor;
        return this;
      },

      limitToLength: function (maxLength) {
        var length = this.length();
        if (length > maxLength) {
          var factor = maxLength / length;
          this.x *= factor;
          this.y *= factor;
        }
        return this;
      },

      dup: function () {
        return mod.create(this.x, this.y);
      },

      scalarMultiply: function (scalar) {
        this.x *= scalar;
        this.y *= scalar;
        return this;
      },

      subtract: function (vector) {
        this.x -= vector.x;
        this.y -= vector.y;
        return this;
      },

      angle: function () {
        return Math.atan2(this.y, this.x);
      }
    };

    var mod = {
      load: function (data) {
        var vector = Object.create(proto);
        vector.x = data.x;
        vector.y = data.y;
        return vector;
      },

      createFromAngleAndLength: function(angle, length) {
        var x = Math.cos(angle) * length;
        var y = Math.sin(angle) * length;
        return this.create(x, y);
      },

      create: function (x, y) {
        var vector = Object.create(proto);
        vector.x = x;
        vector.y = y;
        return vector;
      }
    };

    return mod;
  }
);
