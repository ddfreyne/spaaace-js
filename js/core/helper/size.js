define(
  [],
  function () {
    var proto = {
      dump: function () {
        return {
          width:  this.width,
          height: this.height,
        };
      }
    };

    return {
      load: function (data) {
        var size = Object.create(proto);
        size.width  = data.width;
        size.height = data.height;
        return size;
      },

      create: function (width, height) {
        var size = Object.create(proto);
        size.width  = width;
        size.height = height;
        return size;
      }
    };
  }
);
