# Roadmap

This is a rough order of things.

## **[DONE]** Milestone 1: landing on a planet

Landing on a planet should open the planet screen, with options to enter the market, visit the outfitter for upgrades, or visit the shipyard for new ships.

1. **[DONE]** Ship stationary in center of planet
1. **[DONE]** Console for displaying planet communication
1. **[DONE]** Enforced max velocity for landing
1. **[DONE]** Planet interface is shown with just a “leave” functionality
1. **[DONE]** Space is drawn behind landing screen
1. **[DONE]** Game is paused while landed

## **[DONE]** Milestone 2: trading

There will be a very basic fake economy that allows trading. A more advanced economy is out of scope.

1. **[DONE]** Player starts with a sum of money, shown in the HUD
1. **[DONE]** Planets have commodity prices assigned, shown in the land screen
1. **[DONE]** Player can buy and sell infinite amounts
1. **[DONE]** Ship has limited storage space

## **[DONE]** Milestone 3: multiple systems

There will be large universe with many systems, and the ability to hyperspace jump between them.

1. **[DONE]** Model defines multiple star systems, and marks one system as current
1. **[DONE]** M key opens galactic map, showing systems and marking active system
1. **[DONE]** Allow selecting target system on map
1. **[DONE]** Show target system on HUD
1. **[DONE]** Allow hyperspace jumps, without animation, from anywhere
1. **[DONE]** Add planets to all systems
1. **[DONE]** Add animation to hyperspace jumps
1. **[DONE]** Enter new system at a distance, facing the right way, with velocity
1. **[DONE]** Allow hyperspace jumps only when away from massive objects
1. **[DONE]** Orient before entering hyperspace

## **[DONE]** Milestone 4: main screen, saving and loading

A game should start with a main screen, and use local storage for saving and loading.

1. **[DONE]** Show menu screen after loading, and allow playing by pressing enter
1. **[DONE]** Show button for New game, which creates a new game on click
1. **[DONE]** Store game state in local storage on landing and let continue button load it

## **[DONE]** Milestone 5: other ships

1. **[DONE]** Stationary ships
1. **[DONE]** Different ship types
1. **[DONE]** Randomly moving ships
1. **[DONE]** Ships moving to and from planets
1. **[DONE]** Ships created/destroyed when entering another system or takeoff

## Milestone ?: upgrading

A basic ship should be slow, accelerate slowly, turn slowly, and have limited storage space. Upgrading ships should be possibly in two ways:

* Buying new ships entirely
* Upgrading an existing ship
	* Replace engines
	* Replace side thrusters
