# Spaaace

[Play Spaaace](http://ddfreyne.github.io/spaaace)!

**Work in progress.** Very, very unfinished.

![Spaaace screenshot](screenshot.png)

## Attribution

* [Planet sprites](http://opengameart.org/content/20-planet-sprites) by Justin Nichol, CC-BY 3.0.
* [Spaceship](http://millionthvector.blogspot.de/p/free-sprites.html) by MillionthVector, CC-BY 4.0 International.
